### 项目1.2: 编写一个程序，要求用户输出一个美元数量，然后显示出如何用最少20美元、10美元、5美元和1美元来付款：

        Enter a dollar amount: 93
        $20 bills: 4
        $10 bills: 1
        $5  bills: 0
        $1  bills: 3
