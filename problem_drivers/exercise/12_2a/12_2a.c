#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#define BUFFERSIZE 100

int main(int argc, char *argv)
{
	char buffer[BUFFERSIZE];
	memset(buffer, 0, BUFFERSIZE);
	int length;

    printf("Enter a message: ");
    fgets(buffer, sizeof(buffer), stdin);
    length = strlen(buffer);

    for (int i = 0; i < length - 1; i++)
    {
    	if (isupper(buffer[i]))
			buffer[i] = tolower(buffer[i]);
    }

    for (int i = 0, j = length - 2; i <= j;)
    {
    	if (buffer[i] < 97 || buffer[j] < 97) {
    		if (buffer[i] < 97)
    			i++;
    		if (buffer[j] < 97)
    			j--;
    		continue;
    	}
    	if (buffer[i] != buffer[j]) {
    		printf("Not a Palindrome\n");
    		exit(1);
    	}
    	i++;
    	j--;
    }

    printf("Palindrome\n");
}