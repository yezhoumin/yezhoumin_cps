# 27_1_1 修改guadratic.c(a) Modify guadratic.c(a)

## 题目

(C99) 对 27.4 节的 quadratic.c 程序做如下修改：(a) 让用户输入多项式的系数（变量 a,b,c 的值）。

## 样例

### 样例一

   Please Enter the value of a,b,c: 5,2,1

   root1 = -0.2 + 0.4i
   root2 = -0.2 + -0.4i

### 样例二

   Please Enter the value of a,b,c: -4,3,2

   root1 = -0.425391 + -0i
   root2 = 1.17539 + 0i

## 数据范围

a,b,c 取值范围均在'double'类型内。
输出的 root1, root2 为'%g'类型。