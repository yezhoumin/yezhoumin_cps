#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#define BUFFERSIZE 40

int main(int argc, char *argv[])
{
	char word_1[BUFFERSIZE];
	char word_2[BUFFERSIZE];
	int  words_1[26] = {0};
	int  words_2[26] = {0};
	memset(word_1, 0, BUFFERSIZE);
	memset(word_2, 0, BUFFERSIZE);
	int length;

	printf("Enter first word: ");
	scanf("%s", word_1);
	length = strlen(word_1);
	printf("Enter second word: ");
	scanf("%s", word_2);
    
	for(int i = 0; i < length; i++)
	{
		if (isupper(word_1[i]))
			tolower(word_1[i]);
		if (isupper(word_2[i]))
			tolower(word_2[i]);
		word_1[i] -= 97;
		word_2[i] -= 97;
		words_1[(int)word_1[i]]++;
		words_2[(int)word_2[i]]++;
	}

	for(int i = 0; i < 26; i++)
	{
		if(words_1[i] != words_2[i]){
			printf("The words are not anagrams.\n");
            exit(0);
        }
	}
    printf("The words are anagrams.\n");

}
