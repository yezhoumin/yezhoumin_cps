# 3_4 显示号码 Output phone number

## 题目

编写一个程序，提示用户以 (xxx)xxx-xxxx的格式书瑞电话号码，并以xxx.xxx.xxxx的格式显示该号码。

## 样例

### 样例一

        Enter phone number [(xxx) xxx-xxxx]: (404) 817-6900
        You entered: 404.817.6900

### 样例二

        Enter phone number [(xxx) xxx-xxxx]: (303) 219-2300
        You entered: 303.219.2300

## 数据范围

输入的电话号码为正常范畴内。
按照题目内容中的输出为规范输出。
