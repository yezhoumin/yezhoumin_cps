﻿### 第 13 章编程题 8

修改第 7 章的编程题 5，使其包含如下函数：

     int compute_scrabble_value(const char *word);
函数返回 word 所指向的字符串的拼字值。



