# 5_1 判断数字位数

## 题目

编写一个程序，确定一个数的位数:

    Enter a number: 374
    The number 374 has 3 digits

假设输入的数最多不超过4位。

## 样例

### 样例一

    Enter a number: 45612
    The number 45678 has 5 digits

### 样例二

    Enter a number: 374
    The number 374 has 3 digits

## 数据范围

可以假设输入的数为 `n` 范围为，则 -9999 ≤ n ≤ 9999 。

## 提示

1. 理由if语句进行数的判定。例如，如果数在 0 到 9 之间，那么位数为 1；如果数在 10 到 99 之间，那么位数为2。

2. 按照题目内容中的输出为规范格式进行输出。