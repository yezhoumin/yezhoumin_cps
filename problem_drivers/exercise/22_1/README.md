## 第二十二章

1. 扩展 22.2 节的 canopen.c 程序，以便用户可以把任意数量的文件名放置在命令行中：

        Canopen foo bar baz

    这个程序应该为每个文件分别显示出 can be opened 消息或则 can't be opened 消息。如果一个或多个文件无法打开，程序以 EXIT_FAILURE 状态终止。