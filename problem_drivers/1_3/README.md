### 项目1.3：编程计算第一、第二、第三个月还贷后剩余的贷款金额，输出时保留两位小数：

        Enter amount of loan: 20000.00
        Enter interest rate: 6.0
        Enter monthly payment: 386.66

        Balance remaining after first payment: 19713.34
        Balance remaining after second payment: 19425.25
        Balance remaining after third payment: 19135.71

提示：每个月的贷款余额减去还款金额后，还需要加上贷款余额与月利率的乘积即利息。把用户输入的年利率转换成百分数后再除以12就是月利率。